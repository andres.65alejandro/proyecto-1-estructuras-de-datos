#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Carta{
  char *representacion;
  char *nombre;
  char *familia;
  int valor;
  int Valor_real;
  struct Carta *siguiente;
  struct Carta *anterior;

}Carta;


Carta *primera=NULL;
Carta *ultima=NULL;
Carta *anterior=NULL;


int Espadas= 0;
int Corazones= 13;
int Diamantes= 26;
int Treboles= 39;

char Familia_espada[20]= "♠";
char Familia_corazones[20]="❤";
char Familia_Diamantes[20]="♦";
char Familia_Treboles[20]="♣";

void crearBaraja();
void imprimirBaraja();


void crearBaraja(){

  	int valor= 2;
  	
	int contador;
	contador= 0;
  	while(contador!=52){
		
		 if(valor == 14){
			 valor = valor-13;
			 }
			
		Carta *nueva=(Carta*)malloc(sizeof(Carta));
			
    	if (primera==NULL){
			//inserta valores a nuevo nodo
			nueva->valor= valor;
			nueva->Valor_real= valor + Espadas;
      		nueva->familia= Familia_espada;
      		
      		primera= nueva;
        	ultima= nueva;
        	anterior= nueva;
        	
        	primera->siguiente= NULL;
        	
        	contador++;

    	}else{
			contador ++;
			valor ++;
			
			if(contador<=52){
				nueva->Valor_real= valor + Treboles;
				nueva->familia = Familia_Treboles;
			}
			if(contador<=39){
				nueva->Valor_real=valor + Diamantes;
				nueva->familia = Familia_Diamantes;
			}
			if(contador<=26){
				nueva->Valor_real= valor + Corazones;
				nueva->familia= Familia_corazones;
			}
			if(contador<=13){
				nueva->Valor_real= valor+ Espadas;
				nueva->familia= Familia_espada;
			}
			
			nueva->valor= valor;
			ultima ->siguiente = nueva;
			nueva->siguiente = NULL;
			ultima = nueva;
			anterior =  nueva;
        }
	}
}

void imprimirBaraja(){
	int largo;
	largo= 0;
	
	Carta *temp=(Carta*)malloc(sizeof(Carta));
	temp=primera;
	
	while(temp->siguiente!=NULL){
		printf("\n   %i De %s",temp->valor, temp->familia);
		temp = temp->siguiente;
		largo++;
	}
	printf("\n   %i De %s",temp->valor, temp->familia);
}

void mostrar_carta_actual(struct Carta* baraja){
	
	printf("\n   %i De %s",baraja->valor, baraja->familia);
}

void mostrar_carta_siguiente(struct Carta* baraja){
	struct Carta* siguiente = baraja ->siguiente;
	
	printf("\n   %i De %s",siguiente->valor, siguiente->familia);
}

void mostrar_carta_anterior(struct Carta* baraja){
	
	if(baraja->anterior != NULL){
		struct Carta* anterior = baraja->anterior;
		printf("\n   %i De %s",anterior->valor, anterior->familia);
	}
	else{
		printf("La carta actual es la primera de la baraja");
	}
}


void Menu(){
	int opcion;
	int ciclo =1;
	while(ciclo){
		printf("\n---------------------------------------------------------------------");
		printf("\n(0) Mostrar Carta Actual");
		printf("\n(1) Mostrar siguente carta");
		printf("\n(2) Mostrar carta anterior");
		printf("\n(3) Mostrar Toda la baraja");
		printf("\n(4) Ordenar");
		printf("\n(5) Barajar");
		printf("\n(6) Salir");
		printf("\n---------------------------------------------------------------------");
		printf("\n>> Opcion deseada: ");
		scanf("%d",&opcion);
		switch(opcion){
			case 0:
				mostrar_carta_actual(primera);
				break;
				
			case 1: 
				mostrar_carta_siguiente(primera);
				break;
			case 2:
				mostrar_carta_anterior(primera);
				break;
			
			case 3:	
				imprimirBaraja();
				break;
			case 4:
			
				break;
			case 5:
				break;
				
			case 6: 
				system("PAUSE");
				break;	
			default: ciclo = 0;	
		}
	}
}

int main() {
	crearBaraja();
	Menu();
	return 0;
}
