#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<time.h>

#define RESET_COLOR "\x1b[0m"
#define ROJO_T "\x1b[31m"
#define VERDE_T "\x1b[32m"
#define AZUL_T "\x1b[34m"
#define NEGRO_T "\x1b[30m"
#define BLANCO_F "\x1b[47m"

typedef struct Carta{

  char *representacion;
  char *nombre;
  char *familia;
  int valor;
  int Valor_real;
  struct Carta *siguiente;
  struct Carta *anterior;

}Carta;

Carta *primera = NULL;
Carta *ultima = NULL;
Carta *anterior = NULL;
Carta *primeraDes = NULL;
Carta *ultimaDes = NULL;

int Espadas= 0;
int Corazones= 13;
int Diamantes= 26;
int Treboles= 39;
int Lista_baraja[52];

char Familia_espada[20]= "♠";
char Familia_corazones[20]="❤";
char Familia_Diamantes[20]="♦";
char Familia_Treboles[20]="♣";

void crearBaraja();
void imprimirBaraja();
void mostrar_carta_actual();
void mostrar_carta_siguiente();
void mostrar_carta_anterior();
void ordenar();
void barajar();


void crearBaraja(){

  int valor = 1;
  	
	int contador;
	contador = 0;
  	while(contador != 52){
			if(valor == 13){
				valor = valor-13;
			}
			
			Carta *nueva = (Carta*)malloc(sizeof(Carta));
			
    	if (primera == NULL){
			  //inserta valores a nuevo nodo
			  nueva->valor = valor;
			  nueva->Valor_real = valor + Espadas;
            nueva->familia = Familia_espada;
      		
            primera = nueva;
            ultima = nueva;
            anterior = nueva;
        	
            primera->siguiente = NULL;
        	
            contador++;

    	}else{
			contador ++;
			valor ++;
			
			if(contador<=52){
				nueva->Valor_real= valor + Treboles;
				nueva->familia = Familia_Treboles;
			}
			if(contador<=39){
				nueva->Valor_real=valor + Diamantes;
				nueva->familia = Familia_Diamantes;
			}
			if(contador<=26){
				nueva->Valor_real= valor + Corazones;
				nueva->familia= Familia_corazones;
			}
			if(contador<=13){
				nueva->Valor_real= valor+ Espadas;
				nueva->familia= Familia_espada;
			}
			
			nueva->valor= valor;
			ultima->siguiente = nueva;
			nueva->siguiente = NULL;
			ultima = nueva;
			anterior =  nueva;
      }
	}
}


void imprimirBaraja(){
	
	Carta *temp=(Carta*)malloc(sizeof(Carta));
	temp = primera;
	
	while(temp != NULL){
            
            if (temp->valor + 0 == temp->Valor_real){
                if (temp->valor == 1){
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                printf(NEGRO_T"        |A         |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |    ♠     |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |       A  |"RESET_COLOR"\n");
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                
                temp = temp->siguiente;
             
                }
                if (temp->valor == 13){
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                printf(NEGRO_T"        |K         |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |    ♠     |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |       K  |"RESET_COLOR"\n");
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                   
                  temp = temp->siguiente;
               
                
                }
             
                if (temp->valor == 12){
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                printf(NEGRO_T"        |Q         |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |    ♠     |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |       Q  |"RESET_COLOR"\n");
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                   
                  temp = temp->siguiente;
         
                }
              
                if (temp->valor == 11){
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                printf(NEGRO_T"        |J         |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |    ♠     |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |       J  |"RESET_COLOR"\n");
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                   
                  temp = temp->siguiente;
    
                }
                else{
                
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                printf(NEGRO_T"        |%i          |"RESET_COLOR"\n",temp->valor);
                printf(NEGRO_T"        |           |"RESET_COLOR"\n");
                printf(NEGRO_T"        |           |"RESET_COLOR"\n");
                printf(NEGRO_T"        |    ♠      |"RESET_COLOR"\n");
                printf(NEGRO_T"        |           |"RESET_COLOR"\n");
                printf(NEGRO_T"        |           |"RESET_COLOR"\n");
                printf(NEGRO_T"        |       %i   |"RESET_COLOR"\n",temp->valor);
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                
                temp = temp->siguiente;
                
            if (temp->valor + 26 == temp->Valor_real){
                if (temp->valor == 1){
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                printf(ROJO_T"        |A         |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |    ♦     |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |       A  |"RESET_COLOR"\n");
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                
                temp = temp->siguiente;

                }
                if (temp->valor == 13){
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                printf(ROJO_T"        |K         |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |    ♦     |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |       K  |"RESET_COLOR"\n");
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                   
                  temp = temp->siguiente;
             
                
                }
             
                if (temp->valor == 12){
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                printf(ROJO_T"        |Q         |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |    ♦     |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |       Q  |"RESET_COLOR"\n");
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                   
                  temp = temp->siguiente;
             
                }
              
                if (temp->valor == 11){
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                printf(ROJO_T"        |J         |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |    ♦     |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |       J  |"RESET_COLOR"\n");
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                   
                  temp = temp->siguiente;
              
                }
                else{
                printf(ROJO_T"        ─────────────"RESET_COLOR"\n");
                printf(ROJO_T"        |%i         |"RESET_COLOR"\n",temp->valor);
                printf(ROJO_T"        |           |"RESET_COLOR"\n");
                printf(ROJO_T"        |           |"RESET_COLOR"\n");
                printf(ROJO_T"        |    ♦      |"RESET_COLOR"\n");
                printf(ROJO_T"        |           |"RESET_COLOR"\n");
                printf(ROJO_T"        |           |"RESET_COLOR"\n");
                printf(ROJO_T"        |       %i  |"RESET_COLOR"\n",temp->valor);
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                
                  temp = temp->siguiente;
                }
            }
            
            if (temp->valor + 13 == temp->Valor_real){
                if (temp->valor == 1){
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                printf(ROJO_T"        |A         |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |    ❤     |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |       A  |"RESET_COLOR"\n");
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                
                temp = temp->siguiente;
             
                }
                if (temp->valor == 13){
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                printf(ROJO_T"        |K         |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |    ❤     |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |       K  |"RESET_COLOR"\n");
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                   
                  temp = temp->siguiente;
               
                
                }
             
                if (temp->valor == 12){
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                printf(ROJO_T"        |Q         |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |    ❤     |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |       Q  |"RESET_COLOR"\n");
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                   
                  temp = temp->siguiente;
         
                }
              
                if (temp->valor == 11){
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                printf(ROJO_T"        |J         |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |    ❤     |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |          |"RESET_COLOR"\n");
                printf(ROJO_T"        |       J  |"RESET_COLOR"\n");
                printf(ROJO_T"        ────────────"RESET_COLOR"\n");
                   
                  temp = temp->siguiente;
    
                }
                else{
                
                printf(ROJO_T"        ──────────────"RESET_COLOR"\n");
                printf(ROJO_T"        |%i          |"RESET_COLOR"\n",temp->valor);
                printf(ROJO_T"        |            |"RESET_COLOR"\n");
                printf(ROJO_T"        |            |"RESET_COLOR"\n");
                printf(ROJO_T"        |    ❤       |"RESET_COLOR"\n");
                printf(ROJO_T"        |            |"RESET_COLOR"\n");
                printf(ROJO_T"        |            |"RESET_COLOR"\n");
                printf(ROJO_T"        |       %i   |"RESET_COLOR"\n",temp->valor);
                printf(ROJO_T"        ──────────────"RESET_COLOR"\n");
                temp = temp->siguiente;
     
                }
            }

            if (temp->valor + 39 == temp->Valor_real){
                if (temp->valor == 1){
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                printf(NEGRO_T"        |A         |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |    ♣     |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |       A  |"RESET_COLOR"\n");
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                
                temp = temp->siguiente;

                }
                if (temp->valor == 13){
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                printf(NEGRO_T"        |K         |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |    ♣     |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                printf(NEGRO_T"        |       K  |"RESET_COLOR"\n");
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                   
                  temp = temp->siguiente;
             
                
                }
             
                if (temp->valor == 12){
                  printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                  printf(NEGRO_T"        |Q         |"RESET_COLOR"\n");
                  printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                  printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                  printf(NEGRO_T"        |    ♣     |"RESET_COLOR"\n");
                  printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                  printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                  printf(NEGRO_T"        |       Q  |"RESET_COLOR"\n");
                  printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                   
                  temp = temp->siguiente;
             
                }
              
                if (temp->valor == 11){
                  printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                  printf(NEGRO_T"        |J         |"RESET_COLOR"\n");
                  printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                  printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                  printf(NEGRO_T"        |    ♣     |"RESET_COLOR"\n");
                  printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                  printf(NEGRO_T"        |          |"RESET_COLOR"\n");
                  printf(NEGRO_T"        |       J  |"RESET_COLOR"\n");
                  printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                   
                  temp = temp->siguiente;
              
                }
                else{
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                printf(NEGRO_T"        |%i         |"RESET_COLOR"\n",temp->valor);
                printf(NEGRO_T"        |           |"RESET_COLOR"\n");
                printf(NEGRO_T"        |           |"RESET_COLOR"\n");
                printf(NEGRO_T"        |    ♣      |"RESET_COLOR"\n");
                printf(NEGRO_T"        |           |"RESET_COLOR"\n");
                printf(NEGRO_T"        |           |"RESET_COLOR"\n");
                printf(NEGRO_T"        |       %i  |"RESET_COLOR"\n",temp->valor);
                printf(NEGRO_T"        ────────────"RESET_COLOR"\n");
                
                temp = temp->siguiente;
                }
            }
  }  
}   

        }
        
}
void mostrar_carta_actual(struct Carta* baraja){
  if(baraja->siguiente != NULL){
    printf("\n %i De %s",baraja->valor, baraja->familia);
  }
}

void mostrar_carta_siguiente(struct Carta* baraja){
  if(baraja->siguiente != NULL){
    struct Carta* siguiente = baraja->siguiente;
    printf("\n %i De %s",siguiente->valor, siguiente->familia);
  }
}

void mostrar_carta_anterior(struct Carta* baraja){
  if(baraja->anterior != NULL){
    struct Carta* anterior = baraja->anterior;
		printf("\n   %i De %s",anterior->valor, anterior->familia);
	}
	else{
		printf("La carta actual es la primera de la baraja");
	}
}

void ordenar(struct Carta* primera, struct Carta* ultima){

    Carta *temp=(Carta*)malloc(sizeof(Carta));
    temp = primera;
    
    int i,j,pivot,t1;
    Carta*carta1=NULL;
    Carta*carta2=NULL;
    Carta*carta3=NULL;
   
    if ( primera->Valor_real < ultima->Valor_real){
       
        t1 = 0;
        pivot =  primera->Valor_real; 
        carta1 = primera;
        carta2 = ultima;
        carta3 = primera;
        i = primera->Valor_real;
        j = ultima->Valor_real;
        while(i < j){
            while(i < pivot)
                if(carta1->siguiente !=NULL){
                i = primera->siguiente->Valor_real;
               carta1=carta1->siguiente;
                }
            while(j > pivot)
                if(carta2->anterior->Valor_real !=1){
                j = ultima->anterior->Valor_real;
                carta2=carta2->anterior;
                }
            if(i < j){
                temp = carta1;
                carta1 = carta2;
                carta2 = temp;
                t1 = i;
                i = j;
                j = t1;
          }
        }
    }
    temp = carta3;
    carta3 = carta2;
    carta2 = temp;
    t1 = pivot;
    pivot = j;
    j = t1;
        
    ordenar(carta1,carta2->anterior);
    ordenar(carta2->siguiente,carta2);
    printf("Baraja ya esta ordenanda");
        
 }
        

int aleatorio(){
    int r;
    srand(time(NULL));
    r= rand()%(52-1+1)+1;

    return r;
}

void barajar(){
   int cont=0;
   Carta *temp=(Carta*)malloc(sizeof(Carta));
   temp = primera;
   while(cont!= 52){
		int i=0;
    int num;
		num = aleatorio();
		while(i<= cont){
			if (num== Lista_baraja[i]){
				num= aleatorio();
				i=0;
				}else{
				i++;
				}
	    }
		Lista_baraja[cont]= num;
		printf("%d", cont);
		cont++;
	}
	cont= cont-cont;

	while(temp->siguiente!=NULL){
		temp->Valor_real= Lista_baraja[cont];
		if(temp->Valor_real<=52){
			temp->valor= temp->Valor_real-39;
		}
		if(temp->Valor_real<=39){
			temp->valor= temp->Valor_real-26;

		}
    
    if(temp->Valor_real<=26){
			temp->valor= temp->Valor_real-13;
		}

		if(temp->Valor_real<=13){
			temp->valor= temp->Valor_real;
		}

		temp= temp->siguiente;
		cont++;
	}
	menu();
}


void menu(){
	int opcion;
	int ciclo = 1;
	while(ciclo){
		printf(AZUL_T"\n---------------------------------------------------------------------" RESET_COLOR);
		printf(AZUL_T"\n(1) Mostrar Carta Actual" RESET_COLOR);
		printf(AZUL_T"\n(2) Mostrar siguente carta" RESET_COLOR);
		printf(AZUL_T"\n(3) Mostrar carta anterior" RESET_COLOR);
		printf(AZUL_T"\n(4) Mostrar Toda la baraja" RESET_COLOR);
		printf(AZUL_T"\n(5) Ordenar" RESET_COLOR);
		printf(AZUL_T"\n(6) Barajar" RESET_COLOR);
		printf(AZUL_T"\n(7) Salir" RESET_COLOR);
		printf(AZUL_T"\n---------------------------------------------------------------------" RESET_COLOR);
		printf(AZUL_T"\n>> Opcion deseada: " RESET_COLOR);
		scanf("%d",&opcion);
		switch(opcion){
			case 1:
				mostrar_carta_actual(primera);
				break;
				
			case 2: 
				mostrar_carta_siguiente(primera);
				break;
			case 3:
				mostrar_carta_anterior(primera);
				break;
			
			case 4:	
				imprimirBaraja();
				break;
			case 5:
				ordenar(primera,ultima);
				break;
			case 6:
				barajar();
				break;
				
			case 7: 
				return 0;	
			default: ciclo = 0;	
		};
	}
} 

int main(){
	crearBaraja();
	menu();
	barajar();
	return 0;

}    