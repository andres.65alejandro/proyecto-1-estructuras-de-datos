#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<time.h>

typedef struct Carta{

  char *representacion;
  char *nombre;
  char *familia;
  int valor;
  int Valor_real;
  struct Carta *siguiente;
  struct Carta *anterior;

}Carta;

Carta *primera = NULL;
Carta *ultima = NULL;
Carta *anterior = NULL;

int Espadas= 0;
int Corazones= 13;
int Diamantes= 26;
int Treboles= 39;

char Familia_espada[20]= "♠";
char Familia_corazones[20]="❤";
char Familia_Diamantes[20]="♦";
char Familia_Treboles[20]="♣";

void crearBaraja();
void imprimirBaraja();
void ordenar();
void barajar();


void crearBaraja(){

  int valor = 1;
  	
	int contador;
	contador = 0;
  	while(contador != 52){
			if(valor == 13){
				valor = valor-13;
			}
			
			Carta *nueva = (Carta*)malloc(sizeof(Carta));
			
    	if (primera == NULL){
			//inserta valores a nuevo nodo
			nueva->valor = valor;
			nueva->Valor_real= valor + Espadas;
            nueva->familia= Familia_espada;
      		
            primera= nueva;
            ultima= nueva;
            anterior= nueva;
        	
            primera->siguiente= NULL;
        	
            contador++;

    	}else{
			contador ++;
			valor ++;
			
			if(contador<=52){
				nueva->Valor_real= valor + Treboles;
				nueva->familia = Familia_Treboles;
			}
			if(contador<=39){
				nueva->Valor_real=valor + Diamantes;
				nueva->familia = Familia_Diamantes;
			}
			if(contador<=26){
				nueva->Valor_real= valor + Corazones;
				nueva->familia= Familia_corazones;
			}
			if(contador<=13){
				nueva->Valor_real= valor+ Espadas;
				nueva->familia= Familia_espada;
			}
			
			nueva->valor= valor;
			ultima ->siguiente = nueva;
			nueva->siguiente = NULL;
			ultima = nueva;
			anterior =  nueva;
        }
	}
}

void imprimirBaraja(){
	
	Carta *temp=(Carta*)malloc(sizeof(Carta));
	temp = primera;
	
	while(temp != NULL){
            
            if (temp->valor + 0 == temp->Valor_real){
                if (temp->valor == 1){
                printf("        ────────────\n");
                printf("        |A         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♠     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       A  |\n");
                printf("        ────────────\n");
                
                temp = temp->siguiente;
             
                }
                if (temp->valor == 13){
                printf("        ────────────\n");
                printf("        |K         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♠     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       K  |\n");
                printf("        ────────────\n");
                   
                  temp = temp->siguiente;
               
                
                }
             
                if (temp->valor == 12){
                printf("        ────────────\n");
                printf("        |Q         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♠     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       Q  |\n");
                printf("        ────────────\n");
                   
                  temp = temp->siguiente;
         
                }
              
                if (temp->valor == 11){
                printf("        ────────────\n");
                printf("        |J         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♠     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       J  |\n");
                printf("        ────────────\n");
                   
                  temp = temp->siguiente;
    
                }
                else{
                
                printf("        ────────────\n");
                printf("        |%i          |\n",temp->valor);
                printf("        |           |\n");
                printf("        |           |\n");
                printf("        |    ♠      |\n");
                printf("        |           |\n");
                printf("        |           |\n");
                printf("        |       %i   |\n",temp->valor);
                printf("        ────────────\n");
                
                temp = temp->siguiente;
     
                }
            }
                
            if (temp->valor + 26 == temp->Valor_real){
                if (temp->valor == 1){
                printf("        ────────────\n");
                printf("        |A         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♦     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       A  |\n");
                printf("        ────────────\n");
                
                temp = temp->siguiente;

                }
                if (temp->valor == 13){
                printf("        ────────────\n");
                printf("        |K         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♦     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       K  |\n");
                printf("        ────────────\n");
                   
                  temp = temp->siguiente;
             
                
                }
             
                if (temp->valor == 12){
                printf("        ────────────\n");
                printf("        |Q         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♦     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       Q  |\n");
                printf("        ────────────\n");
                   
                  temp = temp->siguiente;
             
                }
              
                if (temp->valor == 11){
                printf("        ────────────\n");
                printf("        |J         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♦     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       J  |\n");
                printf("        ────────────\n");
                   
                  temp = temp->siguiente;
              
                }
                else{
                printf("        ────────────\n");
                printf("        |%i          |\n",temp->valor);
                printf("        |           |\n");
                printf("        |           |\n");
                printf("        |    ♦      |\n");
                printf("        |           |\n");
                printf("        |           |\n");
                printf("        |       %i   |\n",temp->valor);
                printf("        ────────────\n");
                
                  temp = temp->siguiente;
                }
            }
            if (temp->valor + 13 == temp->Valor_real){
                if (temp->valor == 1){
                printf("        ────────────\n");
                printf("        |A         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ❤     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       A  |\n");
                printf("        ────────────\n");
                
                temp = temp->siguiente;
             
                }
                if (temp->valor == 13){
                printf("        ────────────\n");
                printf("        |K         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ❤     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       K  |\n");
                printf("        ────────────\n");
                   
                  temp = temp->siguiente;
               
                
                }
             
                if (temp->valor == 12){
                printf("        ────────────\n");
                printf("        |Q         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ❤     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       Q  |\n");
                printf("        ────────────\n");
                   
                  temp = temp->siguiente;
         
                }
              
                if (temp->valor == 11){
                printf("        ────────────\n");
                printf("        |J         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ❤     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       J  |\n");
                printf("        ────────────\n");
                   
                  temp = temp->siguiente;
    
                }
                else{
                
                printf("        ────────────\n");printf("        ────────────\n");
                printf("        |%i          |\n",temp->valor);
                printf("        |           |\n");
                printf("        |           |\n");
                printf("        |    ❤      |\n");
                printf("        |           |\n");
                printf("        |           |\n");
                printf("        |       %i   |\n",temp->valor);
                printf("        ────────────\n");printf("        ────────────\n");
                
                temp = temp->siguiente;
     
                }
            }
                
            if (temp->valor + 26 == temp->Valor_real){
                if (temp->valor == 1){
                printf("        ────────────\n");printf("        ────────────\n");
                printf("        |A         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♦     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       A  |\n");
                printf("        ────────────\n");printf("        ────────────\n");
                
                temp = temp->siguiente;

                }
                if (temp->valor == 13){
                printf("        ────────────\n");printf("        ────────────\n");
                printf("        |K         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♦     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       K  |\n");
                printf("        ────────────\n");printf("        ────────────\n");
                   
                  temp = temp->siguiente;
             
                
                }
             
                if (temp->valor == 12){
                printf("        ────────────\n");printf("        ────────────\n");
                printf("        |Q         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♦     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       Q  |\n");
                printf("        ────────────\n");printf("        ────────────\n");
                   
                  temp = temp->siguiente;
             
                }
              
                if (temp->valor == 11){
                printf("        ────────────\n");printf("        ────────────\n");
                printf("        |J         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♦     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       J  |\n");
                printf("        ────────────\n");printf("        ────────────\n");
                   
                  temp = temp->siguiente;
              
                }
                else{
                printf("        ────────────\n");
                printf("        |%i          |\n",temp->valor);
                printf("        |           |\n");
                printf("        |           |\n");
                printf("        |    ♦      |\n");
                printf("        |           |\n");
                printf("        |           |\n");
                printf("        |       %i   |\n",temp->valor);
                printf("        ────────────\n");
                
                temp = temp->siguiente;
                }
            }

            if (temp->valor + 39 == temp->Valor_real){
                if (temp->valor == 1){
                printf("        ────────────\n");
                printf("        |A         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♣     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       A  |\n");
                printf("        ────────────\n");
                
                temp = temp->siguiente;

                }
                if (temp->valor == 13){
                printf("        ────────────\n");
                printf("        |K         |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |    ♣     |\n");
                printf("        |          |\n");
                printf("        |          |\n");
                printf("        |       K  |\n");
                printf("        ────────────\n");
                   
                  temp = temp->siguiente;
             
                
                }
             
                if (temp->valor == 12){
                  printf("        ────────────\n");
                  printf("        |Q         |\n");
                  printf("        |          |\n");
                  printf("        |          |\n");
                  printf("        |    ♣     |\n");
                  printf("        |          |\n");
                  printf("        |          |\n");
                  printf("        |       Q  |\n");
                  printf("        ────────────\n");
                   
                  temp = temp->siguiente;
             
                }
              
                if (temp->valor == 11){
                  printf("        ────────────\n");
                  printf("        |J         |\n");
                  printf("        |          |\n");
                  printf("        |          |\n");
                  printf("        |    ♣     |\n");
                  printf("        |          |\n");
                  printf("        |          |\n");
                  printf("        |       J  |\n");
                  printf("        ────────────\n");
                   
                  temp = temp->siguiente;
              
                }
                else{
                printf("        ────────────\n");
                printf("        |%i          |\n",temp->valor);
                printf("        |           |\n");
                printf("        |           |\n");
                printf("        |    ♣      |\n");
                printf("        |           |\n");
                printf("        |           |\n");
                printf("        |       %i   |\n",temp->valor);
                printf("        ────────────\n");
                
                temp = temp->siguiente;
                }
            }
  }  
}   


void mostrar_carta_actual(struct Carta* baraja){
  if(baraja->siguiente != NULL){
    printf("\n %i De %s",baraja->valor, baraja->familia);
  }
}

void mostrar_carta_siguiente(struct Carta* baraja){
    if(baraja->siguiente != NULL){
	    struct Carta* siguiente = baraja->siguiente;
	    printf("\n %i De %s",siguiente->valor, siguiente->familia);
    }
}

void mostrar_carta_anterior(struct Carta* baraja){
	
	if(baraja->anterior != NULL){
		struct Carta* anterior = baraja->anterior;
		printf("\n   %i De %s",anterior->valor, anterior->familia);
	}
	else{
		printf("La carta actual es la primera de la baraja");
	}

}

void ordenar(Carta*primera, Carta*ultima){
    
    Carta *temp=(Carta*)malloc(sizeof(Carta));
    temp = primera;
    
    int i,j,pivot,t1;
            Carta*carta1=NULL;
            Carta*carta2=NULL;
            Carta*carta3=NULL;
   
    if ( primera->Valor_real < ultima->Valor_real){
       
        t1 = 0;
        pivot =  primera->Valor_real; 
        carta1 = primera;
        carta2 = ultima;
        carta3= primera;
        i = primera->Valor_real;
        j = ultima->Valor_real;
        while(i<j){
            while(i < pivot)
                i=primera->siguiente->Valor_real;
               carta1=carta1->siguiente;
            while(j>pivot)
                j =ultima->anterior->Valor_real;
                carta2=carta2->anterior;
            if(i<j){
                temp = carta1;
                carta1 = carta2;
                carta2 = temp;
                t1=i;
                i =j;
                j=t1;
          }
        }
    }
        temp=carta3;
        carta3=carta2;
        carta2=temp;
        t1=pivot;
        pivot=j;
        j=t1;
        
        ordenar(carta1,carta2->anterior);
        ordenar(carta2->siguiente,carta2);
        printf("Baraja ya esta ordenanda");
      
        
 }


 


void barajar(Carta* mazo){
 /* int i = 0;
  int longitud = strlen(mazo);
	int r;
	Carta* ultima = longitud;

  srand(time(0));
	mazo = calloc(longitud + 1,1);
	while(i < longitud)	{
		r = rand() %longitud;
		if(mazo[r] != '\0')	{
			mazo[i] = mazo[r];
			mazo[r] = 0;
			i++;	
		}
	}
	free(mazo);
	mazo = mazo;
*/
}

void menu(){
	int opcion;
	int ciclo =1;
	while(ciclo){
		printf("\n---------------------------------------------------------------------");
		printf("\n(1) Mostrar Carta Actual");
		printf("\n(2) Mostrar siguente carta");
		printf("\n(3) Mostrar carta anterior");
		printf("\n(4) Mostrar Toda la baraja");
		printf("\n(5) Ordenar");
		printf("\n(6) Barajar");
		printf("\n(7) Salir");
		printf("\n---------------------------------------------------------------------");
		printf("\n>> Opcion deseada: ");
		scanf("%d",&opcion);
		switch(opcion){
			case 1:
				mostrar_carta_actual(primera);
				break;
				
			case 2: 
				mostrar_carta_siguiente(primera);
				break;
			case 3:
				mostrar_carta_anterior(primera);
				break;
			
			case 4:	
				imprimirBaraja();
				break;
			case 5:
			ordenar( primera,ultima);
                        
				break;
			case 6:
				//barajar(Carta* mazo);
				break;
				
			case 7: 
				return ;	
			default: ciclo = 0;	
		}
	}
} 

void main(){
	crearBaraja();
	menu();
  //Carta[52]; /*reo el array de cartas.*/
  /* semilla del generador de nÃƒÂºmeros aleatorios */ 
	//barajar(Carta mazo);
	ordenar(primera,ultima);
	
}


//Referencias 
//DOCPLAYER. (2017). El lenguaje C. 1. Estructuras. Principios de Programación Definicion de estructuras - PDF. Recuperado de https://docplayer.es/20932846-El-lenguaje-c-1-estructuras-principios-de-programacion-1-1-definicion-de-estructuras.html
